package com.blablacar.mowerService;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static com.blablacar.mowerService.Mower.CardinalPoints;


import static org.junit.jupiter.api.Assertions.*;

public class LawnTest {

    private Lawn lawn = new Lawn(5, 5, new HashSet<>());

    @Test
    public void shouldMoveMowerInLawn() {
        Mower mower = new Mower(1, 2, CardinalPoints.N);

        lawn.addMowerToLawn(mower);
        Mower result = lawn.moveMowerInLawn(mower, "RFRFRFRFF");

        assertEquals(1, result.getX());
        assertEquals(3, result.getY());
        assertEquals(CardinalPoints.N, result.getCardinalOrientation());

        assertTrue(lawn.isMowerInLawn(result));
        assertFalse(lawn.isMowerInLawn(mower));
    }

    @Test
    public void shouldMoveMowerInLawnWithLimitsTested() {
        Mower mower = new Mower(3, 3, CardinalPoints.E);

        lawn.addMowerToLawn(mower);
        Mower result = lawn.moveMowerInLawn(mower, "FFRFFRFRRF");

        assertEquals(5, result.getX());
        assertEquals(1, result.getY());
        assertEquals(CardinalPoints.E, result.getCardinalOrientation());
        assertTrue(lawn.isMowerInLawn(result));
        assertFalse(lawn.isMowerInLawn(mower));
    }

    @Test
    public void shouldThrowMowerNotInLawnExceptionIfMowerIsNotInLawn() {
        Mower mower = new Mower(3, 3, CardinalPoints.E);

        RuntimeException e = assertThrows(RuntimeException.class, () -> lawn.moveMowerInLawn(mower, "FFRFFRFRRF"));
        assertEquals("Mower is not in Lawn", e.getMessage());
    }

    @Test
    public void shouldAddMowerToTheLawn() {
        Mower mower = new Mower(1, 1, Mower.CardinalPoints.N);
        lawn.addMowerToLawn(mower);
        assertTrue(lawn.isMowerInLawn(mower));
    }

    @Test
    public void shouldNotAddMowerToTheLawnOutOfEscope() {
        Mower mower = new Mower(-1, -1, Mower.CardinalPoints.N);
        lawn.addMowerToLawn(mower);
        assertFalse(lawn.isMowerInLawn(mower));
    }

}
