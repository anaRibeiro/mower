package com.blablacar.mowerService;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import static com.blablacar.mowerService.Mower.CardinalPoints;
import static com.blablacar.mowerService.Mower.Controls;

public class MowerTest {

    @ParameterizedTest
    @MethodSource("mowerArguments")
    public void shouldMoveMower(Controls control, Mower originCoordinate, Mower expectedCoordinate) {
        originCoordinate.move(control);
        assertEquals(expectedCoordinate.getX(), originCoordinate.getX());
        assertEquals(expectedCoordinate.getY(), originCoordinate.getY());
        assertEquals(expectedCoordinate.getCardinalOrientation(), originCoordinate.getCardinalOrientation());
    }

    private static Stream<Arguments> mowerArguments() {
        return Stream.of(
                arguments(Controls.F,  new Mower(1, 2, CardinalPoints.N), new Mower(1, 3, CardinalPoints.N)),
                arguments(Controls.F,  new Mower(1, 2, CardinalPoints.E), new Mower(2, 2, CardinalPoints.E)),
                arguments(Controls.F,  new Mower(1, 2,  CardinalPoints.S), new Mower(1, 1, CardinalPoints.S)),
                arguments(Controls.F,  new Mower(1, 2, CardinalPoints.W), new Mower(0, 2, CardinalPoints.W)),

                arguments(Controls.L,  new Mower(1, 2, CardinalPoints.N), new Mower(1, 2, CardinalPoints.W)),
                arguments(Controls.L,  new Mower(1, 2, CardinalPoints.S), new Mower(1, 2, CardinalPoints.E)),
                arguments(Controls.L,  new Mower(1, 2, CardinalPoints.E), new Mower(1, 2, CardinalPoints.N)),
                arguments(Controls.L,  new Mower(1, 2, CardinalPoints.W), new Mower(1, 2, CardinalPoints.S)),

                arguments(Controls.R,  new Mower(1, 2, CardinalPoints.N), new Mower(1, 2, CardinalPoints.E)),
                arguments(Controls.R,  new Mower(1, 2, CardinalPoints.S), new Mower(1, 2, CardinalPoints.W)),
                arguments(Controls.R,  new Mower(1, 2, CardinalPoints.E), new Mower(1, 2, CardinalPoints.S)),
                arguments(Controls.R,  new Mower(1, 2, CardinalPoints.W), new Mower(1, 2, CardinalPoints.N))
        );
    }
}
