package com.blablacar.mowerService;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Synchronized;

import java.util.Set;

@Getter(onMethod_ = {@Synchronized})
@AllArgsConstructor
public class Lawn {

    private final int limitX;
    private final int limitY;
    private final Set<Mower> lawnArea;

    public void addMowerToLawn(Mower mower) {
        if (isMowerInsideLawnLimits(mower) && !isMowerInLawn(mower)) {
            lawnArea.add(mower);
        }
    }

    public Mower moveMowerInLawn(Mower initialMower, String instructionsStr) {
        if (!isMowerInLawn(initialMower)) {
            throw new RuntimeException("Mower is not in Lawn");
        }

        String[] controls = instructionsStr.split("");
        Mower destination = initialMower.clone();
        Mower tempMowerPosition = initialMower.clone();

        for (String control : controls) {
            destination.move(Mower.Controls.valueOf(control));
            if (!isMowerInsideLawnLimits(destination) && isMowerInLawn(destination)) {
                destination = tempMowerPosition.clone();
            } else {
                tempMowerPosition = destination.clone();
            }
        }

        lawnArea.remove(initialMower);
        addMowerToLawn(destination);
        return destination;
    }

    public boolean isMowerInLawn(Mower mower) {
        return lawnArea.contains(mower);
    }

    private boolean isMowerInsideLawnLimits(Mower mower) {
        return (mower.getX() >= 0) && (mower.getX() <= limitX
                && (mower.getY() >= 0) && (mower.getY() <= limitY));
    }
}
