package com.blablacar.mowerService;


import java.util.Collections;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        Lawn lawn = new Lawn(5, 5, Collections.synchronizedSet(new HashSet<>()));

        Thread thread1 = new Thread(() -> {
            Mower mower = new Mower(1, 2, Mower.CardinalPoints.N);
            lawn.addMowerToLawn(mower);
            System.out.println("Thread 1 isMowerInLawn: " + lawn.isMowerInLawn(mower));
            System.out.println("Thread 1 initialMower: " + mower);
            System.out.println("Thread 1 resultMower: " + lawn.moveMowerInLawn(mower, "LFLFLFLFF"));
            System.out.println("Thread 1 lawn: " + lawn.getLawnArea());
        });

        Thread thread2 = new Thread(() -> {
            Mower mower = new Mower(3, 3, Mower.CardinalPoints.E);
            lawn.addMowerToLawn(mower);
            System.out.println("Thread 2 isMowerInLawn: " + lawn.isMowerInLawn(mower));
            System.out.println("Thread 2 initialMower: " + mower);
            System.out.println("Thread 2 resultMower: " + lawn.moveMowerInLawn(mower, "FFRFFRFRRF"));
            System.out.println(("Thread 2 lawn: " + lawn.getLawnArea()));
        });

        thread1.start();
        thread2.start();
        System.out.println("Thread Main: " + lawn.getLawnArea());

    }
}
