package com.blablacar.mowerService;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Synchronized;

import java.util.Objects;

@Getter(onMethod_={@Synchronized})
@AllArgsConstructor
public class Mower implements Cloneable {

    private int x;
    private int y;
    private CardinalPoints cardinalOrientation;

    public void move(Controls control) {
        switch(control) {
            case F: moveFront(); break;
            case L: moveLeft(); break;
            case R: moveRight(); break;
        }
    }

    private void moveFront() {
        switch(cardinalOrientation) {
            case N: y += 1; break;
            case S: y -= 1; break;
            case W: x -= 1; break;
            case E: x += 1; break;
        }
    }

    private void moveRight() {
        cardinalOrientation = CardinalPoints.getNextRightPoint(cardinalOrientation);
    }

    private void moveLeft() {
        cardinalOrientation = CardinalPoints.getNextLeftPoint(cardinalOrientation);
    }

    @Override
    public String toString() {
        return  x + " " + y + " " + cardinalOrientation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mower)) return false;
        Mower that = (Mower) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public Mower clone() {
        try {
            return (Mower) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public enum Controls {
        F, L, R;
    }

    public enum CardinalPoints {
        E, N, W, S;

        public static CardinalPoints getNextRightPoint(CardinalPoints cardinalPoint) {
            CardinalPoints next = cardinalPoint;
            switch (cardinalPoint) {
                case N:
                    next = E;
                    break;
                case S:
                    next = W;
                    break;
                case W:
                    next = N;
                    break;
                case E:
                    next = S;
                    break;
            }
            return next;
        }

        public static CardinalPoints getNextLeftPoint(CardinalPoints cardinalPoint) {
            CardinalPoints next = cardinalPoint;
            switch (cardinalPoint) {
                case N:
                    next = W;
                    break;
                case S:
                    next = E;
                    break;
                case W:
                    next = S;
                    break;
                case E:
                    next = N;
                    break;
            }
            return next;
        }

    }
}
